# 2020 PyCon UK proposals

Using https://2019.pyconuk.org/call-proposals/topics-pycon-uk-2019/

- micropython
- lesser known aspects of and the hidden gems in your favourite modules
- what is a coroutine?
- review of Python 3.5 (minimum supported)
- Functional Programming in Python (reprise)
- How Wide is Python? (The Many Different Things That Python Can Do)
- pandas is bad, and you should feel bad for making me use it
- Regular Expression Workshop
- Structure and Interpretation of Python Programs
- Record, Vector, Reference, Map
- Class, Module, Namespace
- Python and Socialism


## lesser known aspects of and the hidden gems in your favourite modules

My 3 favourite modules are:

- struct
- operator
- itertools


## Record, Vector, Reference, Map

Have you ever wondered how data is laid out in your computer's memory?
How does data become structured, and become _datastructures_?
Ingredients:
a record consisting of a fixed number of fields;
and a vector consisting of many repeated elements of the same type.
To make: any arbitrary datastructure.
The speaker will model these structures using back-of-napkin
style box-and-stick diagrams.

## pandas is bad and you should feel bad for making me use it

`pandas`, the least Pythonic module, is designed in a way to bring
misery to your programming life.
Like a fix of sugary over-caffeinated energy drinks it may bring a
temporary boost to your data science, but
it is unsustainable in many ways.
It encourages unsustainable programming practices;
it encourages unsustainable memory management;
and it create an unsustainable population of so-called Python
programmers that need retraining.
Sustainability comes slowly, and with craft, and with pain.
Please enter the dojo.

## micropython

`micropython` is a gem.
It's an alternate implementation of the Python programming language
that runs on tiny microprocessors that you can buy for a few bucks.
This workshop is an introduction to physical computing
whre we do something cool with the NodeMCU board that I haven't
thought of yet.

## Regular Expression Workshop

Regular expressions are fun and they can solve many problems.
They are also available in many different programming systems,
so while you may know them in Python, you might be delighted
to discover that they can also be used in shell or ruby.

The workshop consists of:
- introduction to the theory and practice of regular expressions;
- craft session to create your own regular expressions from
  quality raw materials;
- discussion and critical analysis of some "found" regular expressions;
- "show and tell" to share the stories of your own favourite
  regular expressions;
- regular expressions as a life style, addiction, self-help.


## Functional Programming in Python

My 2007 slides are https://drj11.files.wordpress.com/2007/09/slides.pdf

My 2008 slides are https://drj11.files.wordpress.com/2008/04/slides.pdf
